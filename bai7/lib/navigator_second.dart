import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class NavigationSecond extends StatefulWidget {
  const NavigationSecond({Key? key}) : super(key: key);

  @override
  State<NavigationSecond> createState() => _NavigationSecondState();
}

class _NavigationSecondState extends State<NavigationSecond> {
  Color? color;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Navigation Second Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
                child: Text('Red'),
                onPressed: () {
                  color = Colors.red[700];
                  Navigator.pop(context, color);
                }),
            ElevatedButton(
                child: Text('Green'),
                onPressed: () {
                  color = Colors.green[700];
                  Navigator.pop(context, color);
                }),
            ElevatedButton(
                child: Text('Blue'),
                onPressed: () {
                  color = Colors.blue[700];
                  Navigator.pop(context, color);
                }),
          ],
        ),
      ),
    );
  }
}
