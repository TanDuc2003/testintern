import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class NavigationDiaLog extends StatefulWidget {
  const NavigationDiaLog({Key? key}) : super(key: key);

  @override
  State<NavigationDiaLog> createState() => _NavigationDiaLogState();
}

class _NavigationDiaLogState extends State<NavigationDiaLog> {
  Color? color = Colors.blue[700];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      appBar: AppBar(
        title: Text("Navigation Dialog Screen"),
      ),
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              _showColorDialog(context);
            },
            child: const Text("Chang Color")),
      ),
    );
  }

  _showColorDialog(BuildContext context) async {
    color = null;
    await showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: const Text("Very important question"),
          content: Text("Please choose a color"),
          actions: [
            TextButton(
              onPressed: () {
                color = Colors.red[700];
                Navigator.pop(context, color);
              },
              child: const Text("Red"),
            ),
            TextButton(
              onPressed: () {
                color = Colors.green[700];
                Navigator.pop(context, color);
              },
              child: const Text("Green"),
            ),
            TextButton(
              onPressed: () {
                color = Colors.blue[700];
                Navigator.pop(context, color);
              },
              child: const Text("Blue"),
            ),
          ],
        );
      },
    );
    setState(() {
      color = color;
    });
  }
}
