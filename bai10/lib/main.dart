import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const PackageScreen(),
    );
  }
}

class PackageScreen extends StatefulWidget {
  const PackageScreen({Key? key}) : super(key: key);

  @override
  State<PackageScreen> createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  final TextEditingController txtHeight = TextEditingController();
  final TextEditingController txtWeight = TextEditingController();
  double? result;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Package APP"),
      ),
      body: Column(
        children: [
          AppTextField(controller: txtHeight, lable: "Height"),
          AppTextField(controller: txtWeight, lable: "Weight"),
          ElevatedButton(
            onPressed: () {
              double? width = double.tryParse(txtWeight.text);
              double? height = double.tryParse(txtHeight.text);
              double res = height! * width!;
              setState(() {
                result = res as double;
              });
            },
            child: const Text("Calculete Area"),
          ),
          const Padding(
            padding: EdgeInsets.all(24),
          ),
          Text('$result'),
        ],
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  AppTextField({Key? key, required this.controller, this.lable})
      : super(key: key);
  final TextEditingController controller;
  String? lable;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
          hintText: lable,
        ),
      ),
    );
  }
}
