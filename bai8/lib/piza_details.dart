import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class PizaDetails extends StatefulWidget {
  const PizaDetails({Key key}) : super(key: key);

  @override
  State<PizaDetails> createState() => _PizaDetailsState();
}

class _PizaDetailsState extends State<PizaDetails> {
  final TextEditingController txtId = TextEditingController();
  final TextEditingController txtName = TextEditingController();
  final TextEditingController txtDescription = TextEditingController();
  final TextEditingController txtPrice = TextEditingController();
  final TextEditingController txtImageUrl = TextEditingController();
  String postResult = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pizza Details"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TextField(
                controller: txtId,
                decoration: InputDecoration(hintText: 'Insert ID'),
              ),
              SizedBox(
                height: 24,
              ),
              TextField(
                controller: txtName,
                decoration: InputDecoration(hintText: 'Insert Pizza Name'),
              ),
              SizedBox(
                height: 24,
              ),
              TextField(
                controller: txtDescription,
                decoration: InputDecoration(hintText: 'Insert Description'),
              ),
              SizedBox(
                height: 24,
              ),
              TextField(
                controller: txtPrice,
                decoration: InputDecoration(hintText: 'Insert Price'),
              ),
              SizedBox(
                height: 24,
              ),
              TextField(
                controller: txtImageUrl,
                decoration: InputDecoration(hintText: 'Insert Image Url'),
              ),
              SizedBox(
                height: 48,
              ),
              ElevatedButton(
                onPressed: () {},
                child: const Text("Send Post"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
