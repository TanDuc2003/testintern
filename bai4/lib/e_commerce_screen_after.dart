import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ECommerceScreen extends StatelessWidget {
  const ECommerceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppbar(),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            _buildTogglebar(context),
            Image.network(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcpsiTO-fvlWzSddcYy9zC6eVWW0gvz8MQc8F8FLHS1hjtxEm5K8ibEEdtUGvjBk3Oclg&usqp=CAU'),
            DealButtons(),
            _buildProducTitle(context),
          ],
        ),
      ),
    );
  }

  Widget _buildTogglebar(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          _buildToggleItem(context, 'Recommended', selected: true),
          _buildToggleItem(context, 'Formal Wear'),
          _buildToggleItem(context, 'Casual Wear'),
        ],
      ),
    );
  }

  Widget _buildToggleItem(BuildContext context, String text,
      {bool selected = false}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 17,
          color: selected ? null : Colors.amber,
        ),
      ),
    );
  }

  Container _buildProducTitle(BuildContext context) {
    return Container(
      height: 200,
      color: Theme.of(context).cardColor,
      child: Row(
        children: [
          Expanded(
            child: Image.network(
              'https://store-images.s-microsoft.com/image/apps.54739.14266069062940839.0386a7c7-7a53-4e48-b184-3c1b8af04617.60917b6c-f77d-4aef-baf4-b8aa891c5889?mode=scale&q=90&h=720&w=1280',
              fit: BoxFit.fitWidth,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Lorem Ipsum',
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                  const Text(
                    'Dolor sit amet, consectetur adipiscing elit. Quisque faucibus.',
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  AppBar _buildAppbar() {
    return AppBar(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      leading: const Padding(
        padding: EdgeInsets.all(20.0),
        child: Icon(Icons.home),
      ),
      title: const Text('Let\'s go shopping!'),
      actions: const [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Icon(Icons.shopping_cart),
        ),
      ],
    );
  }
}

class DealButtons extends StatelessWidget {
  const DealButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 15,
        ),
        Row(
          children: const [
            DealButton(
              colors: Colors.orangeAccent,
              text: "Best Sellers",
            ),
            SizedBox(
              width: 10,
            ),
            DealButton(
              colors: Colors.blue,
              text: "Daily Deals",
            )
          ],
        )
      ],
    );
  }
}

class DealButton extends StatelessWidget {
  final String text;
  final Color colors;

  const DealButton({
    Key? key,
    required this.colors,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          color: colors,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
