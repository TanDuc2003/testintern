import 'package:flutter/material.dart';

class FlexScreen extends StatelessWidget {
  const FlexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flexible and Expaned"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ..._header(context, "Expaned"),
          _buildExpaned(context),
          ..._header(context, "Flexible"),
          _buildFlexible(context),
          Expanded(child: Container()),
          _buildFooter(context),
          Expanded(child: Container()),
        ],
      ),
    );
  }

  Iterable<Widget> _header(BuildContext context, String text) {
    return [
      const SizedBox(
        height: 20,
      ),
      Text(
        text,
        style: Theme.of(context).textTheme.headline4,
      )
    ];
  }

  Widget _buildFooter(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.yellow,
            borderRadius: BorderRadius.circular(40),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 15.0,
              horizontal: 30,
            ),
            child: Text(
              "Pinned to the Bottom",
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildExpaned(BuildContext context) {
    return SizedBox(
      height: 100,
      child: Row(
        children: const [
          LabeledContainer(
            width: 100,
            height: double.infinity,
            colors: Colors.green,
            text: '100',
            textColor: Colors.pink,
          ),
          Expanded(
            child: LabeledContainer(
              colors: Colors.purple,
              text: "The Remaider",
              width: double.infinity,
              textColor: Colors.white,
            ),
          ),
          LabeledContainer(
            width: 40,
            height: double.infinity,
            colors: Colors.green,
            text: '40',
            textColor: Colors.pink,
          )
        ],
      ),
    );
  }
}

Widget _buildFlexible(BuildContext context) {
  return SizedBox(
    height: 100,
    child: Row(
      children: const [
        Flexible(
          flex: 1,
          child: LabeledContainer(
            colors: Colors.orange,
            text: '25%',
            textColor: Colors.pink,
            width: double.infinity,
          ),
        ),
        Flexible(
          flex: 1,
          child: LabeledContainer(
            colors: Colors.deepOrange,
            text: '25%',
            textColor: Colors.pink,
            width: double.infinity,
          ),
        ),
        Flexible(
          flex: 2,
          child: LabeledContainer(
            colors: Colors.blue,
            text: '50%',
            textColor: Colors.pink,
            width: double.infinity,
          ),
        )
      ],
    ),
  );
}

class LabeledContainer extends StatelessWidget {
  final double width;
  final double height;
  final Color colors;
  final String text;
  final Color textColor;
  const LabeledContainer({
    Key? key,
    required this.width,
    this.height = double.infinity,
    required this.colors,
    required this.text,
    required this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      color: colors,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            color: textColor,
            fontSize: 20,
          ),
        ),
      ),
    );
  }
}
