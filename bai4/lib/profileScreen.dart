import 'package:bai4/Start.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.network(
              "https://goigon.com/wp-content/uploads/2020/10/top-7-bai-bien-dep-nhat-viet-nam-avt.png"),
          Transform.translate(
            offset: Offset(0, 100),
            child: Column(
              children: [
                _buildProfileImage(context),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Wolfram Barkovich',
                        style: TextStyle(
                            fontSize: 35, fontWeight: FontWeight.w600),
                      ),
                      StarRating(value: 5),
                      _builDetailsRow("Age", "4"),
                      _builDetailsRow("Status", "Goodboy"),
                      _buildActions(context),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProfileImage(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      child: ClipOval(
        child: Image.network(
          "https://www.princeton.edu/sites/default/files/styles/half_2x/public/images/2022/02/KOA_Nassau_2697x1517.jpg?itok=iQEwihUn",
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}

Widget _builDetailsRow(String heading, String value) {
  return Row(
    children: [
      Text(
        '$heading: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      Text(value),
    ],
  );
}

Widget _buildActions(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      _buildIcon(Icons.restaurant, "feed"),
      _buildIcon(Icons.favorite, "Pet"),
      _buildIcon(Icons.directions_walk, "Walk"),
    ],
  );
}

Widget _buildIcon(IconData icon, String text) {
  return Padding(
    padding: const EdgeInsets.all(20.0),
    child: Column(
      children: [
        Icon(
          icon,
          size: 40,
        ),
        Text(text),
      ],
    ),
  );
}
