import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Deeptree extends StatelessWidget {
  const Deeptree({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const <Widget>[
                FlutterLogo(),
                Text("Flutter is amazing!"),
              ],
            ),
            Expanded(
              child: Container(
                color: Colors.amber,
              ),
            ),
            const Text('Its all widgets'),
            const Text('Let\'s find out how deep the rabbit hole goes.'),
          ],
        ),
      ),
    );
  }
}
