import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/animation.dart';

class DismissibleScreen extends StatefulWidget {
  const DismissibleScreen({Key? key}) : super(key: key);

  @override
  State<DismissibleScreen> createState() => _DismissibleScreenState();
}

class _DismissibleScreenState extends State<DismissibleScreen> {
  final List<String> sweets = [
    'Petit Four',
    'Cupcake',
    'Donut',
    'Éclair',
    'Froyo',
    'Gingerbread ',
    'Honeycomb ',
    'Ice Cream Sandwich',
    'Jelly Bean',
    'KitKat'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dismissible Example'),
      ),
      body: ListView.builder(
          itemCount: sweets.length,
          itemBuilder: (context, index) {
            return OpenContainer(
              transitionDuration: const Duration(seconds: 1),
              transitionType: ContainerTransitionType.fade,
              openBuilder: (context, action) {
                return Scaffold(
                  appBar: AppBar(
                    title: Text(sweets[index]),
                  ),
                  body: Center(
                    child: Column(
                      children: [
                        Container(
                          width: 200,
                          height: 200,
                          child: const Icon(
                            Icons.cake,
                            color: Colors.orange,
                          ),
                        ),
                        Text(sweets[index]),
                      ],
                    ),
                  ),
                );
              },
              closedBuilder: (context, openContainer) {
                return Dismissible(
                  key: Key(sweets[index]),
                  child: ListTile(
                    title: Text(sweets[index]),
                    trailing: const Icon(Icons.cake),
                    onTap: () {
                      openContainer();
                    },
                  ),
                  onDismissed: (direction) {
                    sweets.removeAt(index);
                  },
                );
              },
            );
          }),
    );
  }
}
