import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'details_screen.dart';

class ListScreen extends StatelessWidget {
  final List<String> drinks = ['Coffe', 'Tea', 'Capuchino', 'Espresso'];
  ListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hero Animation'),
      ),
      body: ListView.builder(
        itemCount: drinks.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Hero(
              tag: 'cup$index',
              child: const Icon(Icons.free_breakfast),
            ),
            title: Text(drinks[index]),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailsScreen(index: index),
                  ));
            },
          );
        },
      ),
    );
  }
}
