import 'package:flutter/material.dart';

class FadeTransitions extends StatefulWidget {
  const FadeTransitions({Key? key}) : super(key: key);

  @override
  State<FadeTransitions> createState() => _FadeTransitionsState();
}

class _FadeTransitionsState extends State<FadeTransitions>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  late final Animation<double> animation;

  initState() {
    AnimationController(vsync: this, duration: const Duration(seconds: 3));
    animation = CurvedAnimation(
      parent: controller!,
      curve: Curves.easeIn,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // controller.forward();
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Fade Transition Recipe"),
        ),
        body: Center(
          child: FadeTransition(
            opacity: animation,
            child: Container(
              width: 200,
              height: 200,
              color: Colors.purple,
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }
}
