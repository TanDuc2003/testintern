import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class AniamtedListScreen extends StatefulWidget {
  const AniamtedListScreen({Key? key}) : super(key: key);

  @override
  State<AniamtedListScreen> createState() => _AniamtedListScreenState();
}

class _AniamtedListScreenState extends State<AniamtedListScreen>
    with TickerProviderStateMixin {
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  final List<int> _items = [1, 2, 3, 4, 5];
  int counter = 0;

  // controller
  late AnimationController _controller;

  // animation
  late Animation<double> _animation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Animated List"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
      body: AnimatedList(
        key: listKey,
        initialItemCount: _items.length,
        itemBuilder: (context, index, animation) {
          return fadeListTile(context, index, animation);
        },
      ),
    );
  }

  fadeListTile(BuildContext context, int index, Animation animation) {
    int item = _items[index];
    return FadeTransition(
      opacity: Tween(begin: 0.0, end: 1.0).animate(_animation),
      child: Card(
        child: ListTile(
          title: Text('Pizza ' + item.toString()),
          onTap: () {
            removePizza(index);
          },
        ),
      ),
    );
  }

  removePizza(int index) {
    int animationIndex = index;
    if (index == _items.length - 1) animationIndex--;
    listKey.currentState?.removeItem(
      index,
      (context, animation) => fadeListTile(context, animationIndex, animation),
      duration: Duration(seconds: 1),
    );
    _items.removeAt(index);
  }

  insertPizza() {
    listKey.currentState?.insertItem(
      _items.length,
      duration: Duration(seconds: 1),
    );
    _items.add(++counter);
  }
}
