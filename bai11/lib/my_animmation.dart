import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class MyAnimation extends StatefulWidget {
  const MyAnimation({Key? key}) : super(key: key);

  @override
  State<MyAnimation> createState() => _MyAnimationState();
}

class _MyAnimationState extends State<MyAnimation> {
  final List<Color> colors = [
    Colors.red,
    Colors.green,
    Colors.yellow,
    Colors.blue,
    Colors.orange
  ];
  final List<double> sizes = [100, 125, 150, 175, 200];
  final List<double> tops = [0, 50, 100, 150, 200];
  int iteration = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("animated Container"),
        actions: [
          IconButton(
            onPressed: () {
              iteration < colors.length - 1 ? iteration++ : iteration = 0;
              setState(() {
                iteration = iteration;
              });
            },
            icon: Icon(Icons.run_circle),
          ),
        ],
      ),
      body: Center(
        child: AnimatedContainer(
          duration: const Duration(seconds: 1),
          color: colors[iteration],
          width: sizes[iteration],
          height: sizes[iteration],
          margin: EdgeInsets.only(
            top: tops[iteration],
          ),
        ),
      ),
    );
  }
}
