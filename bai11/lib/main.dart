import 'package:bai11/dismissible.dart';
import 'package:bai11/shape_animation.dart';
import 'package:flutter/material.dart';

import 'animatelist.dart';
import 'fade_transition.dart';
import 'list_screen.dart';
import 'my_animmation.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ShapeAnimation(),
    );
  }
}
