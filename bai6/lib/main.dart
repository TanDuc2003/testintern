import 'package:bai6/plan_provider.dart';
import 'package:bai6/view/plancreator_Screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(PlanProvider(child: MasterPlanApp()));

class MasterPlanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.purple),
      home: PlanCreatorScreen(),
    );
  }
}
